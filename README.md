<p>
    <img src="https://gitlab.com/nsm-templates/parchment-site/raw/master/site/img/nsm.png" width='120'/>
</p>

=====

Copyright (c) 2015-present [Nicholas Ham](https://n-ham.com).

This is an official (GitLab) repository for nifty-site-manager site repository template `markdown-site`.

Demo:
\[[github](https://nsm-templates.github.io/markdown-site)\] \[[gitlab](https://nsm-templates.gitlab.io/markdown-site)\]

Mirrors:
\[[bitbucket](https://bitbucket.org/nsm-templates/markdown-site)\] \[[github](https://github.com/nsm-templates/markdown-site)\] \[[gitlab](https://gitlab.com/nsm-templates/markdown-site)\]

